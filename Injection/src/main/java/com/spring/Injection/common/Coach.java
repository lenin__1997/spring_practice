package com.spring.Injection.common;

public interface Coach {
	
	String getDailyWorkout(); 
}
