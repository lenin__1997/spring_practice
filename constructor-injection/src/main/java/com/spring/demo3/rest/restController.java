package com.spring.demo3.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class restController {
	@GetMapping("/")
	public String sayHello() {
		return "Hello ";
	}
	
	@Value("${night.sky}")
	private String night;	

	@Value("${daysky}")
	private String day;	
	
	@GetMapping("/weather")
	public String getInfo() {
		return "In Morning you can see "+day+" at night "+night;
	}
	
	@GetMapping("/fortune")
	public String hi() {
		return "Today is your lucky day";
	}
}
 