package com.spring.demo3.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class restController {
	@GetMapping("/")
	public String sayHello() {
		return "Hello ";
	}
	
	@GetMapping("/workout")
	public String getDailyWorkout() {
		return "Run hard 5km a day";
	}
	
	@GetMapping("/fortune")
	public String hi() {
		return "Today is your lucky day";
	}
}
 