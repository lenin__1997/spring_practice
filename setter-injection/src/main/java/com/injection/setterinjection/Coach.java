package com.injection.setterinjection;

public interface Coach {
	
	String getDailyWorkout(); 
}
