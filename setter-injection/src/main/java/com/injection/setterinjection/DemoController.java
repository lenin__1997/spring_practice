package com.injection.setterinjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.spring.Injection.common.Coach;

@RestController
public class DemoController {
	
	private Coach myCoach;
//	private Coach anotherCoach;	
	
	@Autowired
	public void setCoach(
			@Qualifier("swimCoach") Coach theCoach) {
		System.out.println("In Constructor : " +getClass().getSimpleName());

		myCoach =  theCoach;
//		anotherCoach = toAnotherCoach;
	}
	
	@GetMapping("/dailyworkout")
	public String getDailyWorkout() {
		return myCoach.getDailyWorkout();
//		return "Comparing beans : myCoash==anotherCoash  "+ (myCoach==anotherCoach);
	}
	
	
	
	@GetMapping("/")
	public String onIntro() {
		return "Working";
	}
}
