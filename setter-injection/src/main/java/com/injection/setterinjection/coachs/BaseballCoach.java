package com.injection.setterinjection.coachs;

import org.springframework.stereotype.Component;

import com.injection.setterinjection.Coach;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

@Component
//@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BaseballCoach implements Coach {
	
	public BaseballCoach() {
		System.out.println("In Constructor : " +getClass().getSimpleName());
	}

	@PostConstruct
	public void initMethod() {
		System.out.println("In doMyStartupStuff " + getClass().getSimpleName());
	}
	
	@PreDestroy
	public void destroyMethod() {
		System.out.println("In doMyCleanupStuff " + getClass().getSimpleName());
	}
	
	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "30 minutes Baseball Practice";
	}

}
