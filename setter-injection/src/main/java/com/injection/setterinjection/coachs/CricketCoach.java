package com.injection.setterinjection.coachs;

import org.springframework.stereotype.Component;

import com.injection.setterinjection.Coach;

@Component
public class CricketCoach implements Coach {
	
	public CricketCoach() {
		System.out.println("In Constructor : " +getClass().getSimpleName());
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Practice fast bowling for  15minutes";
	}

} 
