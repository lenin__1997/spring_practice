package com.injection.setterinjection.coachs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.injection.setterinjection.Coach;

@Configuration
public class SportConfig {
	@Bean()
	public Coach swimCoach() {
		return new SwimCoach();
	}
}
