package com.injection.setterinjection.coachs;

//import org.springframework.stereotype.Component;

import com.injection.setterinjection.Coach;

public class SwimCoach implements Coach {
	
	public SwimCoach() {
		System.out.println(" In Constructor : "+getClass().getSimpleName());
	}
	
	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Swim 100 meters as warm up";
	}
	
	
}
