package com.injection.setterinjection.coachs;

import org.springframework.stereotype.Component;

import com.injection.setterinjection.Coach;


@Component
public class TennisCoach implements Coach {

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "30 minutes practice for Tennis";
	}

}
